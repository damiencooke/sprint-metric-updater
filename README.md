# sprint-metric-updater

This application will take a CSV export from Jira and modify it so that it can be used in a sprint metric spreadsheet.

To run place a csv file into the file-to-parse folder. There needs to be only one file in that folder for it to work correctly

Then run the following command:
- npm test
