module.exports = {
  filename: "ModifiedSprintMetircs.xlsx",
  columnKeyArray: {
    'Completed Date': 'A',
    'Start date(optional)': 'B',
    'Issue Type(optional)': 'C',
    'Story Point': 'D',
    'Planned vs unplanned': 'E',
    'Id(optional)': 'F',
    'Description(optional)': 'G'
  },
  replacementIdsObject: [
    { oldValue: "Resolved", newValue: "Completed Date" },
    { oldValue: "Customfield\\(StartDateandTime\\)", newValue: "Start date(optional)" },
    { oldValue: "IssueType", newValue: "Issue Type(optional)" },
    { oldValue: "Summary", newValue: "Description(optional)" },
    { oldValue: "Issuekey", newValue: "Id(optional)"},
    { oldValue: "Customfield\\(StoryPoints\\)", newValue: "Story Point"}
  ]
}
