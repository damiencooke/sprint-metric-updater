const moment = require('moment')
const { format } = require('date-format-parse')

module.exports = (jsonArray) => {
  const dateCorrections = ['Updated', 'Completed Date', 'Start date(optional)']

  const newArrayOfObj = Object.keys(jsonArray).map( obj => {
    for(const dateCorrection of dateCorrections) {
      if(jsonArray[obj][dateCorrection]){
      jsonArray[obj][dateCorrection] = 
      format(jsonArray[obj][dateCorrection].slice(1, -1), 'DD/MM/YYYY')
      }
    }
  })

  return jsonArray
}

