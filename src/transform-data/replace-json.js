module.exports = (jsonArray, replacementIdsObject) => {
  return JSON.parse(replacementIdsObject.reduce((json, {
    oldValue,
    newValue
    }) => {
    return json.replace(
      new RegExp(`"${oldValue}"`, 'g'),
      () => `"${newValue}"`
    );
  }, JSON.stringify(jsonArray)));
}