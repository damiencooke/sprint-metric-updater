const initReplaceJson = require('./transform-data/replace-json')
const initParseDates = require('./transform-data/parse-dates')
const initWriteDataToFile = require('./write-data/writeDataToFile')
const initGetColumnKey = require('./utils/getColumnKey')
const initWriteToSheet = require('./write-data/writeToSheet')
const initWriteHeadersToFile = require('./write-data/writeHeadersToFile')
const initWriteToFile = require('./write-data/writeToFile')
const initGetFilename = require('./utils/getFilename')
const csvToJson = require('convert-csv-to-json').fieldDelimiter(';')
const csvFilePath = './file-to-parse/JIRA DWP 2021-01-08T14_41_19+0000.csv'

const getFilename = initGetFilename()

const { columnKeyArray, filename, replacementIdsObject } = require('../constants')

const json = csvToJson.getJsonFromCsv(`./file-to-parse/${getFilename}`)

const replaceJson = initReplaceJson(json, replacementIdsObject)

const parseDates = initParseDates(replaceJson)

const writeToSheet = initWriteToSheet()

const getColumnKey = initGetColumnKey(columnKeyArray)

const jsonElementKeys = Object.keys(parseDates[0])

const writeHeadersToFile = initWriteHeadersToFile(jsonElementKeys, getColumnKey)

const writeDataToFile = initWriteDataToFile(parseDates, getColumnKey)

const writeToFile = initWriteToFile(writeToSheet, writeHeadersToFile, writeDataToFile, filename)