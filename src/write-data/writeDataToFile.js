const XlsxPopulate = require('xlsx-populate')

module.exports = (json, getColumnKey, colNumber = 2) => (workbook, writeToSheet, filename) => {
  for (const jsonElement of json) {
    const jsonElementKey = Object.keys(jsonElement)
    for (let x = 0; x < jsonElementKey.length; x++) {
      writeToSheet(workbook, getColumnKey(jsonElementKey[x]), colNumber, jsonElement[jsonElementKey[x]])
    }
    colNumber++
  }
}