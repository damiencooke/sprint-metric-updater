const XlsxPopulate = require('xlsx-populate')

module.exports = (writeToSheet, writeHeadersToFile, writeDataToFile, filename) => {
  XlsxPopulate.fromBlankAsync()
    .then(workbook => {
      writeHeadersToFile(workbook, writeToSheet, filename)
      writeDataToFile(workbook, writeToSheet, filename)

      return workbook.toFileAsync(filename);

    })
}