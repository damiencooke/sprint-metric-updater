
module.exports = (jsonElementKeys, getColumnKey) => (workbook, writeToSheet, filename) => {
  for (const jsonElementKey of jsonElementKeys) {
    writeToSheet(workbook, getColumnKey(jsonElementKey), "1", jsonElementKey)
  }
  return workbook.toFileAsync(filename)
}