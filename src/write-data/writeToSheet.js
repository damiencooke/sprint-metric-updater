module.exports = () => (workbook, cellAlpha, cellNumeric, value) => {
  if (typeof cellAlpha != "undefined") {
    workbook.sheet("Sheet1").cell(cellAlpha + cellNumeric).value(value)
  }
}
